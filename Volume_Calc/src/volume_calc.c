/* *****************************************************************************

 FILE_NAME:     Volume_Calc.c
 DESCRIPTION:   App Feature for Volume and Density compensation
 DESIGNER:      Plínio Barbosa da Silva
 CREATION_DATE: 11/2018

 VERSION:       1.0
 ***************************************************************************** */

/* *****************************************************************************
 *        INCLUDES (and DEFINES for INCLUDES)
 ***************************************************************************** */
#include "volume_calc.h"

#include <stdint.h>
#include <math.h>
#include <time.h>
#include <tgmath.h>



/* *****************************************************************************
 *        PRIVATE VARIABLES
 *******************************************************************************/

/*******************************************************************************
 *        PRIVATE CONSTATNS
 *******************************************************************************/
#define	CTE_P1	(double) ((9.0/5.0) * 0.9990420)
#define	CTE_P2	(double) (9.0/5.0)
#define	CTE_P3	(double) ((81.0/25.0) * 0.9990420)
#define	CTE_P4	(double) (81.0/25.0)
/*
 * | Faixa de D20/4   | a1 * 10^6 | a2 * 10^6 | b1 * 10^6 | b2 * 10^6 |
 * |------------------|-----------|-----------|-----------|-----------|
 * | até 0,498        | -2462     | +3215     | -10,14    | +17,38    |
 * | de 0,498 a 0,518 | -2391     | +3074     | -8,41     | +13,98    |
 * | de 0,518 a 0,539 | -2294     | +2 887    | -8,39     | +13,87    |
 * | de 0,539 a 0,559 | -2146     | +2615     | -5,46     | +8,55     |
 * | de 0,559 a 0,579 | -1920     | +2214     | -5,51     | +8,55     |
 * | de 0,579 a 0,600 | -2358     | +2962     | -12,25    | +20,15    |
 * | de 0,600 a 0,615 | -1361     | +1300     | -0,49     | +0,6      |
 * | de 0,615 a 0,635 | -1237     | +1100     | -0,49     | +0,6      |
 * | de 0,635 a 0,655 | -1077     | +850      | -0,49     | +0,6      |
 * | de 0,655 a 0,675 | -1011     | +750      | -0,49     | +0,6      |
 * | de 0,675 a 0,695 | -977      | +700      | -0,49     | +0,6      |
 * | de 0,695 a 0,746 | -1005     | +740      | -0,49     | +0,6      |
 * | de 0,746 a 0,766 | -1238     | +1050     | -0,49     | +0,6      |
 * | Je 0,766 a 0,786 | -1084     | +850      | -0,49     | +0,6      |
 * | de 0,786 a 0,806 | -965      | +700      | -0,49     | +0,6      |
 * | de 0,806 a 0,826 | -843,5    | +550      | -0,49     | +0,6      |
 * | de 0,826 a 0,846 | -719      | +400      | -0,49     | +0,6      |
 * | de 0,846 a 0,871 | -617      | +2 80     | -0,49     | +0,6      |
 * | de 0,871 a 0,896 | -512      | +160      | -0,49     | +0,6      |
 * | de 0,896 a 0,996 | -394,8    | +30       | -0,49     | +0,6      |
 * | acima de 0,996   | -542,6    | +177,8    | +2,31     | -2,20     |
 *
 *
 */
const double Faixa_Densidade_D_20_4[19][2] =
{
		{0.498 , 0.518},
		{0.518 , 0.539},
		{0.539 , 0.559},
		{0.559 , 0.579},
		{0.579 , 0.600},
		{0.600 , 0.615},
		{0.615 , 0.635},
		{0.635 , 0.655},
		{0.655 , 0.675},
		{0.675 , 0.695},
		{0.695 , 0.746},
		{0.746 , 0.766},
		{0.766 , 0.786},
		{0.786 , 0.806},
		{0.806 , 0.826},
		{0.826 , 0.846},
		{0.846 , 0.871},
		{0.871 , 0.896},
		{0.896 , 0.996}
};

const double Densidade_a1[] = {-2462.0, -2391.0, -2294.0, -2146.0, -1920.0, -2358.0, -1361.0, -1237.0, -1077.0, -1011.0, -977.0, -1005.0, -1238.0, -1084.0, -965.0, -843.5, -719.0, -617.0, -512.0, -394.8, -542.6};
const double Densidade_a2[] = {+3215.0, +3074.0, +2887.0, +2615.0, +2214.0, +2962.0, +1300.0, +1100.0, +850.0, +750.0, +700.0, +740.0, +1050.0, +850.0, +700.0, +550.0, +400.0, +280.0, +160.0, +30.0, +177.8};

const double Densidade_b1[] ={-10.14, -8.41, -8.39, -5.46, -5.51, -12.25, -0.49, -0.49, -0.49, -0.49, -0.49, -0.49, -0.49, -0.49, -0.49, -0.49, -0.49, -0.49, -0.49, -0.49, +2.31};
const double Densidade_b2[] ={+17.38, +13.98, +13.87, +8.55, +8.55, +20.15, +0.6, +0.6, +0.6, +0.6, +0.6, +0.6, +0.6, +0.6, +0.6, +0.6, +0.6, +0.6, +0.6, +0.6, -2.20};


double Temperature_Correction_Coef_a[9] =
{
		0.0,
		-0.148759,
		-0.267408,
		1.080760,
		1.269056,
		-4.089591,
		-1.871251,
		7.438081,
		-3.536296
};

double Round_Increment_Table[] =
{
		(double)    0.1,
		(double)    0.0001,
		(double)    0.1,
		(double)    0.1,
		(double)    0.05,
		(double)    1.0,
		(double)    5.0,
		(double)    0.05,
		(double)    0.0000001,
		(double)    0.0000002,
		(double)    0.00001,
		(double)    0.001,
		(double)    0.0001,
		(double)    0.01,
		(double)    0.00001,
		(double)    0.00001
};

const double Comodity_Density_Range[7][6] =
{
		/*                             Rho60_Min | Rho60_Max | D_Alpha |  K0       |  K1     |  K2  */
		/* Crude Oil            */    {610.6     , 1163.5    , 2.0     , 341.0957  , 0.0     ,  0.0},
		/* Fuel Oil             */    {838.3127  , 1163.5    , 1.3     , 103.8720  , 0.2701  ,  0.0},
		/* Jet Fuel             */    {787.5195  , 838.3127  , 2.0     , 330.3010  , 0.0     ,  0.0},
		/* Transition Zone      */    {770.3520  , 787.5195  , 8.5     , 1489.0670 , 0.0     , -0.00186840},
		/* Gasolines            */    {610.6     , 770.3520  , 1.5     , 192.4571  , 0.2438  ,  0.0},
		/* Lubrificating Oil    */    {800.9     , 1163.5    , 1.0     , 0.0       , 0.34878 ,  0.0},
		/* Non Specified        */	  {0.0       , 0.0       , 0.0     , 0.0       , 0.0     ,  0.0}
};

Volume_Calc_Output_Data_t API_Calc_Inverse_Method(Volume_Calc_Input_Data_t Input_Data)
{
	Volume_Calc_Output_Data_t Calculated_Data;
	double Rho0;
	double Delta_Rho0_m;
	double Delta_Rho60_m;
	double Rho60_m;
	double E_m;
	double D_t_m;
	double D_p_m;

	double Aux1, Aux2;

	double Commodity_K0;
	double Commodity_K1;
	double Commodity_K2;
	double Commodity_D_Alpha;
	uint32_t Iteraction = 0;

	Rho60_m = Input_Data.Rho60;

	if(Input_Data.Pressure < 0)
		Input_Data.Pressure  = 0;
	// STEP 1: Analize the input variables limits.
	if( ((Input_Data.Temperature_F >=API_TEMPERATURE_MIN_F) && (Input_Data.Temperature_F <= API_TEMPERATURE_MAX_F)) &&
			((Input_Data.Pressure >= API_PRESSURE_MIN_PSIG) && (Input_Data.Pressure <= API_PRESSURE_MAX_PSIG))    )
	{
		// STEP2: Use the especified density Rho0 as the initial guess for Rho60
		Rho0 = Rho60_m;

		if((Input_Data.Rho60<API_DENSITY60_MIN_Kg_m3) || (Input_Data.Rho60>API_DENSITY60_MAX_Kg_m3))
		{
			Calculated_Data.Calc_Status     = API_CALC_STATUS_INPUT_RANGE_ERROR;
			return Calculated_Data;
		}
		if(Input_Data.Commodity_Type != COMMODITY_TYPE_NON_SPECIFIED)
		{
			Commodity_D_Alpha = Comodity_Density_Range[Input_Data.Commodity_Type][2];
			Commodity_K0      =	Comodity_Density_Range[Input_Data.Commodity_Type][3];
			Commodity_K1      = Comodity_Density_Range[Input_Data.Commodity_Type][4];
			Commodity_K2      = Comodity_Density_Range[Input_Data.Commodity_Type][5];

			/* STEP3: Calculate the correction factor due to temperature and pressure Ctpl(m)
		using the current guess for the density at base conditions Rho60(m). Do not round
		the value of Ctpl(m) and retain Alpha60 as Aplha60(m)
			 */
			for(Iteraction = 0; Iteraction < 15; Iteraction++)
			{
				Calculated_Data.Shifted_Temperature_F = API_Convert_ITS90_To_IPTS68(Input_Data.Temperature_F);
				Calculated_Data.Shifted_Rho_F = API_Transfer_Rho60_To_Rho_No_Alpha60(Input_Data.Delta60, Rho60_m, Commodity_K0, Commodity_K1, Commodity_K2);
				Calculated_Data.Alpha60 = API_Calculate_Alpha60(Commodity_K0, Commodity_K1, Commodity_K2, Calculated_Data.Shifted_Rho_F);
				Calculated_Data.Delta_t = API_Calculate_Delta_t(Calculated_Data.Shifted_Temperature_F);
				Calculated_Data.Ctl = API_Calc_C_tl(Calculated_Data.Alpha60, Input_Data.Delta60, Calculated_Data.Delta_t);
				Calculated_Data.Fp = API_Calc_F_p(Calculated_Data.Shifted_Temperature_F, Calculated_Data.Shifted_Rho_F);
				Calculated_Data.Cpl = API_Calc_C_pl(Calculated_Data.Fp, Input_Data.Pressure);
				Calculated_Data.Ctpl = API_Calc_C_tpl(Calculated_Data.Ctl, Calculated_Data.Cpl);

				Delta_Rho0_m = Rho0 - (Rho60_m*Calculated_Data.Ctpl);
				if(Delta_Rho0_m > DELTA_RHO_CONVERGENCE_VALUE)
				{
					Aux1 = (2.0 * Calculated_Data.Cpl * Input_Data.Pressure * Calculated_Data.Fp)*(7.93920 + (0.02326 * Calculated_Data.Shifted_Temperature_F));
					Aux2 = (Rho60_m * Rho60_m);
					D_p_m = -(Aux1 / Aux2);

					D_t_m = Commodity_D_Alpha * Calculated_Data.Alpha60 * (Calculated_Data.Shifted_Temperature_F - 60.0) * (1.0 + (1.6*Calculated_Data.Alpha60*(Calculated_Data.Shifted_Temperature_F - 60.0)));

					E_m = (Rho0 / (Calculated_Data.Ctl * Calculated_Data.Cpl)) - Rho60_m;

					Delta_Rho60_m = E_m / (1.0 + D_t_m + D_p_m);
					//TODO: Verificar Range de Rho60


					Rho60_m = Rho60_m + Delta_Rho60_m;
				}
				else
				{

					Calculated_Data.Ctpl_Rounded 	= API_Round_Value(Calculated_Data.Ctpl, Round_Increment_CTPL);
					Calculated_Data.Volume_Base_60 	= Input_Data.Volume_t_p * Calculated_Data.Ctpl_Rounded;
					Calculated_Data.Rho60 			= Rho60_m;
					Calculated_Data.Calc_Status     = API_CALC_STATUS_OK;
					return Calculated_Data;
				}
			}

			if(Iteraction >= 15)
				Calculated_Data.Calc_Status     = API_CALC_STATUS_CONVERGENCE_ERROR;
			return Calculated_Data;
		}
		else
		{
			// TODO: Implement the calcs using the alpha60 parameters
			//printf("Implement the calcs using the alpha60 parameters!");
		}
	}
	else
	{
		Calculated_Data.Calc_Status     = API_CALC_STATUS_INPUT_RANGE_ERROR;
		return Calculated_Data;
	}

	return Calculated_Data;
}

Volume_Calc_Output_Data_t API_Calc_Direct_Method(Volume_Calc_Input_Data_t Input_Data)
{
	Volume_Calc_Output_Data_t Calculated_Data;
	double Commodity_K0;
	double Commodity_K1;
	double Commodity_K2;
	// STEP 1: Analize the input variables limits.
	if( ((Input_Data.Temperature_F>=API_TEMPERATURE_MIN_F) && (Input_Data.Temperature_F<=API_TEMPERATURE_MAX_F)) &&
			((Input_Data.Pressure>=API_PRESSURE_MIN_PSIG)      && (Input_Data.Pressure<=API_PRESSURE_MAX_PSIG)) &&
			((Input_Data.Rho60>=API_DENSITY60_MIN_Kg_m3)       && (Input_Data.Rho60<=API_DENSITY60_MAX_Kg_m3)))
	{
		if(Input_Data.Commodity_Type < COMMODITY_TYPE_NON_SPECIFIED)
		{
			Commodity_K0      =	Comodity_Density_Range[Input_Data.Commodity_Type][3];
			Commodity_K1      = Comodity_Density_Range[Input_Data.Commodity_Type][4];
			Commodity_K2      = Comodity_Density_Range[Input_Data.Commodity_Type][5];

			// STEP 2: Shift the temperature
			Calculated_Data.Shifted_Temperature_F = API_Convert_ITS90_To_IPTS68(Input_Data.Temperature_F);

			// STEP 3: Shift RHO60 to IPTS68
			Calculated_Data.Shifted_Rho_F = API_Transfer_Rho60_To_Rho_No_Alpha60(Input_Data.Delta60 ,
					Input_Data.Rho60   ,
					Commodity_K0       ,
					Commodity_K1       ,
					Commodity_K2);

			// STEP 4: Determine Alpha60
			Calculated_Data.Alpha60 = API_Calculate_Alpha60(Commodity_K0 ,
					Commodity_K1 ,
					Commodity_K2 ,
					Calculated_Data.Shifted_Rho_F);

			// STEP 5: Calculate the temperature correction Factor
			Calculated_Data.Delta_t = API_Calculate_Delta_t(Calculated_Data.Shifted_Temperature_F);
			Calculated_Data.Ctl = API_Calc_C_tl(Calculated_Data.Alpha60, Input_Data.Delta60, Calculated_Data.Delta_t);

			// STEP 6: Calculate the scaled compressibility factor Fp
			Calculated_Data.Fp = API_Calc_F_p(Calculated_Data.Shifted_Temperature_F, Calculated_Data.Shifted_Rho_F);

			// STEP 7: Calculate the pressure correction factor Cpl
			Calculated_Data.Cpl = API_Calc_C_pl(Calculated_Data.Fp, Input_Data.Pressure);

			// STEP 8: Calculate the volume correction factor Ctpl
			Calculated_Data.Ctpl = API_Calc_C_tpl(Calculated_Data.Ctl, Calculated_Data.Cpl);
			Calculated_Data.Ctpl_Rounded = API_Round_Value(Calculated_Data.Ctpl, Round_Increment_CTPL);

			// STEP 9: Calculate volume at base conditions
			Calculated_Data.Rho            = Input_Data.Rho60      * Calculated_Data.Ctpl;
			Calculated_Data.Volume_Base_60 = Input_Data.Volume_t_p * Calculated_Data.Ctpl;

			Calculated_Data.Calc_Status     = API_CALC_STATUS_OK;
		}
		else
		{
			// STEP 2: Shift the temperature
			Calculated_Data.Shifted_Temperature_F = API_Convert_ITS90_To_IPTS68(Input_Data.Temperature_F);

			// STEP 3: Shift RHO60 to IPTS68
			Calculated_Data.Shifted_Rho_F = API_Transfer_Rho60_To_Rho(Input_Data.Alpha60, Input_Data.Delta60, Input_Data.Rho60);

			// STEP 4: Determine Alpha60
			//Alpha60 = API_Calculate_Alpha60(K0, K1, K2, Shifted_Rho_F);

			// STEP 5: Calculate the temperature correction Factor
			Calculated_Data.Delta_t = API_Calculate_Delta_t(Calculated_Data.Shifted_Temperature_F);
			Calculated_Data.Ctl = API_Calc_C_tl(Input_Data.Alpha60, Input_Data.Delta60, Calculated_Data.Delta_t);

			// STEP 6: Calculate the scaled compressibility factor Fp
			Calculated_Data.Fp = API_Calc_F_p(Calculated_Data.Shifted_Temperature_F, Calculated_Data.Shifted_Rho_F);

			// STEP 7: Calculate the pressure correction factor Cpl
			Calculated_Data.Cpl = API_Calc_C_pl(Calculated_Data.Fp, Input_Data.Pressure);

			// STEP 8: Calculate the volume correction factor Ctpl
			Calculated_Data.Ctpl = API_Calc_C_tpl(Calculated_Data.Ctl, Calculated_Data.Cpl);
			Calculated_Data.Ctpl_Rounded = API_Round_Value(Calculated_Data.Ctpl, Round_Increment_CTPL);

			// STEP 9: Calculate volume at base conditions
			Calculated_Data.Rho            = Input_Data.Rho60      * Calculated_Data.Ctpl;
			Calculated_Data.Volume_Base_60 = Input_Data.Volume_t_p * Calculated_Data.Ctpl;

			Calculated_Data.Calc_Status     = API_CALC_STATUS_OK;
		}
	}
	else
	{
		Calculated_Data.Calc_Status     = API_CALC_STATUS_INPUT_RANGE_ERROR;
	}

	return Calculated_Data;
}

/* -----------------------------------------------------------------------------
Comp_Density()
        This routine performs the compensation of density of a fluid over the
        temperature influence. This function uses the Brazilian normalization.
--------------------------------------------------------------------------------
Input:  Density_t -> Observed Density of the fluid.
Output: Temperature_t -> Observed temperature of the fluid.
Return: Compensated fluid Density over the temperature of 20 Degree Celcius
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

    B. técn. PETROBRAS , Rio de Janeiro, 38 (3/ 4): 127-137, jul./dez. 1995
----------------------------------------------------------------------------- */
Volume_Calc_Output_Data_t BR_Calc_Method(Volume_Calc_Input_Data_t Input_Data)
{
	Volume_Calc_Output_Data_t Calculated_Data;

	double Cte_a1, Cte_a2, Cte_b1, Cte_b2;
	double Var_P1, Var_P2, Var_P3, Var_P4;
	double Delta_Temperatura;
	uint8_t Faixa_Densidade;
	double Aux1, Aux2;
	double Delta_Temp_2;


	if(Input_Data.Density_t_p < 0.498)			/* | até 0,498        | -2462     | +3215     | -10,14    | +17,38    | */
	{
		Faixa_Densidade	= 0;
	}
	else if(Input_Data.Density_t_p > 0.996) 	/* | acima de 0,996   | -542,6    | +177,8    | +2,31     | -2,20     | */
	{
		Faixa_Densidade	= 20;
	}
	else
	{
		for(Faixa_Densidade = 0; Faixa_Densidade < 19; Faixa_Densidade ++)
		{

			if((Input_Data.Density_t_p >= Faixa_Densidade_D_20_4[Faixa_Densidade][0]) && (Input_Data.Density_t_p < Faixa_Densidade_D_20_4[Faixa_Densidade][1]))
			{
				Faixa_Densidade +=1;
				break;
			}
		}
	}
	Cte_a1 = Densidade_a1[Faixa_Densidade] / 1E6;
	Cte_a2 = Densidade_a2[Faixa_Densidade] / 1E6;
	Cte_b1 = Densidade_b1[Faixa_Densidade] / 1E6;
	Cte_b2 = Densidade_b2[Faixa_Densidade] / 1E6;

	/* Denominador comum a todas variaveis P */
	Aux2 = (8.0*Cte_a2) + (64.0*Cte_b2) + 1;

	/* Determinando P1 */
	Aux1 = Cte_a1 * (16.0*Cte_a2 + 192.0*Cte_b2 + 1) - (16.0*Cte_b1)*(4.0*Cte_a2 + 1.0);
	Var_P1 = CTE_P1 * (Aux1 / Aux2);


	/* Determinando P2 */
	Aux1 = (Cte_a2 + (16.0 * Cte_b2));
	Var_P2 = CTE_P2 * (Aux1 / Aux2);

	/* Determinando P3 */
	Aux1 = -(8.0*Cte_a1*Cte_b2 - (8*Cte_a2 + 1)*Cte_b1);
	Var_P3 = CTE_P3 *(Aux1 / Aux2);

	/* Determinando P4 */
	Aux1 = Cte_b2;
	Var_P4 = CTE_P4 * (Aux1 / Aux2);

	/* Determinando Densidade Compensada */
	Delta_Temperatura = (Input_Data.Temperature_C - 20);

	Aux1 = Input_Data.Density_t_p - Var_P1*Delta_Temperatura - Var_P3*Delta_Temperatura*Delta_Temperatura;
	Aux2 = 1 + Var_P2*Delta_Temperatura + Var_P4*Delta_Temperatura*Delta_Temperatura;

	Calculated_Data.Density_20_4  = Aux1/Aux2;

	Delta_Temp_2 = Delta_Temperatura * Delta_Temperatura;

	Calculated_Data.Volume_20  = 0;
	Calculated_Data.Volume_20 += 1;
	Calculated_Data.Volume_20 += CTE_P2 * Delta_Temperatura;
	Calculated_Data.Volume_20 += CTE_P4 * Delta_Temp_2;
	Calculated_Data.Volume_20 += ((CTE_P1 * Delta_Temperatura) + (CTE_P3 * Delta_Temp_2)) / Calculated_Data.Density_20_4;
	Calculated_Data.Volume_20 = Input_Data.Volume_t_p * Calculated_Data.Volume_20;

	return Calculated_Data;
}

/* -----------------------------------------------------------------------------
API_Convert_ITS90_To_IPTS-68()
        This routine performs the conversion of temperature ITS90 to IPTS68
--------------------------------------------------------------------------------
Input:  Temperature_ITS_90 -> Temperature on ITS 90 in Degree Fahrenheit.
Return: Converted temperature to IPTS 68 in Degree Fahrenheit.
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 27

NOaRMA PGD 209
----------------------------------------------------------------------------- */

double API_Convert_ITS90_To_IPTS68(double Temperature_ITS_90_Fahrenheit)
{
	double Scaled_Temperature;
	double Delta_Temp;
	double Temperature_IPTS_68_Celcius;
	double Temperature_IPTS_68_Fahrenheit;
	double Temperature_ITS_90_Celcius;
	//int8_t Aux_Counter;

	Temperature_ITS_90_Celcius = (double)((Temperature_ITS_90_Fahrenheit - 32.0) / 1.8);
	Scaled_Temperature =  (double)(Temperature_ITS_90_Celcius / 630.0);

	Delta_Temp = (((((((- 3.536296  * Scaled_Temperature + 7.438081) * Scaled_Temperature
			- 1.871251) * Scaled_Temperature - 4.089591) * Scaled_Temperature
			+ 1.269056) * Scaled_Temperature + 1.080760) * Scaled_Temperature
			- 0.267408) * Scaled_Temperature - 0.148759) * Scaled_Temperature;

	Temperature_IPTS_68_Celcius = Temperature_ITS_90_Celcius - Delta_Temp;

	Temperature_IPTS_68_Fahrenheit = (1.8 * Temperature_IPTS_68_Celcius) + 32.0;

	return Temperature_IPTS_68_Fahrenheit;
}

/* -----------------------------------------------------------------------------
API_Transfer_Rho60_To_Rho()
        This routine performs the conversion of Rh60 to Rho*
                    (Knowing the Alpha 60)
--------------------------------------------------------------------------------
Input:  Alpha60 -> Thermal expansion factor over 60F
        Delta60 -> Temperature shift value.
        Rho60   -> Density at 60F
Return: Rho*    -> Base density Shifted to IPS-68 60F basis
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 32
----------------------------------------------------------------------------- */

double API_Transfer_Rho60_To_Rho(double Alpha60, double Delta60, double Rho60)
{
	double Rho;
	double Aux;

	Aux = (0.5*Alpha60*Delta60)*(1+0.4*Alpha60);
	Rho = Rho60 * exp(Aux);

	return Rho;
}

/* -----------------------------------------------------------------------------
API_Transfer_Rho60_To_Rho_No_Alpha60()
        This routine performs the conversion of Rh60 to Rho*
                    (Unknowing the Alpha 60)
--------------------------------------------------------------------------------
Input:  Delta60 -> Temperature shift value.
        Rho60   -> Density at 60F
        K0, K1, K2  -> Coeficients of the fluid
Return: Rho*    -> Base density Shifted to IPS-68 60F basis
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 32
----------------------------------------------------------------------------- */

double API_Transfer_Rho60_To_Rho_No_Alpha60(double Delta60, double Rho60, double K0,
		double K1, double K2)
{
	double Rho;
	double A, B;
	double Num, Den, Aux;
	A = (Delta60 / 2.0) * ((K0 + (Rho60*K1) + (Rho60*Rho60*K2))/(Rho60*Rho60));
	B = ((2.0 * K0) + (K1 * Rho60)) / (K0 + (K1 + K2*Rho60)* Rho60);
	Aux = A*(1.0 + 0.8*A);
	Num = expf(Aux) - 1.0;
	Den = 1.0 + A*B*(1.0 + 1.6*A);
	Rho = Rho60 * (1 + (Num/Den));


	return Rho;
}

/* -----------------------------------------------------------------------------
API_Calculate_Alpha60()
        This routine performs the calc of Alpha60 using the commodity group

--------------------------------------------------------------------------------
Input:  K0, K1, K2  -> Coeficients of the fluid
        Rho*    -> Base density Shifted to IPS-68 60F basis

Return: Alpha60     -> Thermal expansion factor over 60F
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calculate_Alpha60(double K0, double K1, double K2, double Rho)
{
	double alpha60;


		alpha60 = ((K0/(Rho * Rho)) + (K1/Rho)) + K2;


	return alpha60;
}

/* -----------------------------------------------------------------------------
API_Calculate_Delta_t()

        This routine performs the calc of Delta_t
--------------------------------------------------------------------------------
Input:  t           -> Alternate Temperature shifted to IPTS-68 basis (F)

Return: Delta_t     -> Alternate Temperature minus the base temperature of 60F
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calculate_Delta_t(double t)
{
	double Delta_t;

	Delta_t = t - (double)60.0068749;

	return Delta_t;
}

/* -----------------------------------------------------------------------------
API_Calc_C_tl()
        This routine calculate the correction factor due to temperature C_tl

--------------------------------------------------------------------------------
Input:  Delta60 -> Temperature shift value.
        Alpha60 -> Thermal expansion factor over 60F
        Delta_t -> Alternate Temperature minus the base temperature of 60F

Return: C_tl    -> Correction factor due to temperature
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calc_C_tl(double Alpha60, double Delta60, double Delta_t)
{
	double C_tl;
	double Aux;

	Aux = -Alpha60 * Delta_t * ((double)1.0 + (double)0.8*Alpha60*(Delta_t + Delta60));
	C_tl = exp(Aux);

	return C_tl;
}

/* -----------------------------------------------------------------------------
API_Calc_F_p()
        This routine calculate the scaled compressibility factor F_p.

--------------------------------------------------------------------------------
Input:  t       -> Alternate Temperature shifted to IPTS-68 basis (F)
        Rho*    -> Base density Shifted to IPS-68 60F basis

Return: F_p     -> Scaled compressibility factor
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calc_F_p(double t, double Rho)
{
	double F_p;
	double Aux;

	Aux = (double)-1.9947 + (double)(0.0013427*t) + (((double)793920.0 + (double)2326.0*t)/(Rho * Rho));

	F_p = exp(Aux);

	return F_p;
}

/* -----------------------------------------------------------------------------
API_Calc_C_pl()
        This routine calculate the pressure correction factor C_pl.

--------------------------------------------------------------------------------
Input:  F_p     -> Scaled compressibility factor.
        P       -> Alternate Pressure (psig)

Return: C_pl     -> Pressure correction factor
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calc_C_pl(double F_p, double P)
{
	double C_pl;


	C_pl = (double)1.0 / ((double)1.0 - (pow(10, -5) * F_p * P));

	return C_pl;
}

/* -----------------------------------------------------------------------------
API_Calc_C_tpl()
        This routine calculate the combined temperature and pressure
 correction factor C_tpl.

--------------------------------------------------------------------------------
Input:  C_tl     -> Correction factor due to temperature.
        C_pl     -> Pressure correction factor

Return: C_tpl     -> Combined temperature and pressure
 correction factor
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calc_C_tpl(double C_tl, double C_pl)
{
	double C_tpl;

	C_tpl = C_tl * C_pl;

	return C_tpl;
}

/* -----------------------------------------------------------------------------
API_Round_Value()
        This routine round  the combined temperature and pressure
 correction factor C_tpl.

--------------------------------------------------------------------------------
Input:  C_tl     -> Correction factor due to temperature.
        C_pl     -> Pressure correction factor

Return: round_value     -> Combined temperature and pressure
 correction factor
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */



double API_Round_Value(double value, Round_Increment_Var_Type_t Variable_Type)
{
	double Fractional_Part, Integer_Part;
	double round_value;
	double Delta;
	double Y;
	uint32_t Trunc_Y;
	Delta = Round_Increment_Table[Variable_Type];
	Y = (double)(fabs(value) / Delta);
	Fractional_Part = (double)modf(Y, &Integer_Part);
	if(Fractional_Part != (double)0.5000)
		Trunc_Y = (double)(trunc(Y + (double)0.5));
	else
		Trunc_Y = trunc(Y);
	//if(Trunc_Y%2 != 0)
	//    Trunc_Y += 1;

	round_value = Trunc_Y * Delta;
	if(value < 0)
		round_value *= (double)-1.0;
	return round_value;
}
