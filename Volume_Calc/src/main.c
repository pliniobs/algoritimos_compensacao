#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "volume_calc.h"

enum
{
	API_DIRECT_METHOD,
	API_INVERSE_METHOD,
	BR_METHOD,
}Vol_Calc_Type = API_DIRECT_METHOD;




int main(int argc, char** argv) {

	Volume_Calc_Input_Data_t In_Data;
	Volume_Calc_Output_Data_t Out_Data;

	Vol_Calc_Type = API_DIRECT_METHOD;

	switch(Vol_Calc_Type)
	{
	case BR_METHOD:
		In_Data.Density_t_p   = 0.6257;
		In_Data.Temperature_C = 12.5;
		In_Data.Volume_t_p    = 86.424;
		printf("********* BR Method  *******");
		Out_Data = BR_Calc_Method(In_Data);
		printf("Observed Density and Temperature: Density_20_4=%f, Volume_20=%f\n", Out_Data.Density_20_4, Out_Data.Volume_20);

		break;
	case API_INVERSE_METHOD:
		//Input Variables for the interactive method (Generalized Crude Oil)
		In_Data.Rho60 			= 823.7;	// Observed Density (Kg/m^3)
		In_Data.Pressure 		= -5.0; 	// Pressure at which the observed density was measured (psig)
		In_Data.Temperature_F 	= 80.3; 	// Temperature at which the observed density was measured (F)
		In_Data.Volume_t_p 		= 1000.0; 	// Volume at observed conditions (any valid unit is acepted)
		In_Data.Delta60 		= 0.01374979547;
		In_Data.Commodity_Type	= COMMODITY_TYPE_CRUDE_OIL;
		Out_Data = API_Calc_Inverse_Method(In_Data);
		break;

	case API_DIRECT_METHOD:
		In_Data.Rho60 			= 793.404;	// Observed Density (Kg/m^3)
		In_Data.Pressure 		= 0.0; 	    // Pressure at which the observed density was measured (psig)
		In_Data.Temperature_F 	= 77.0; 	// Temperature at which the observed density was measured (F)
		In_Data.Volume_t_p 		= 100.0; 	// Volume at observed conditions (any valid unit is acepted)
		In_Data.Delta60 		= 0.01374979547;
		In_Data.Commodity_Type	= COMMODITY_TYPE_CRUDE_OIL;
		Out_Data = API_Calc_Direct_Method(In_Data);

		break;

	}

	return (EXIT_SUCCESS);
}
