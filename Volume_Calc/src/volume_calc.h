/* *****************************************************************************

 FILE_NAME:     CalendarGpsSynch.h
 DESCRIPTION:   App Feature for Calendar synchronism using GPS info
 DESIGNER:      Andre F. N. Dainese
 CREATION_DATE: 07/2016

 VERSION:       2.0
***************************************************************************** */
#ifndef VOLUME_CALC_H_
#define VOLUME_CALC_H_

/* *****************************************************************************
 *        INCLUDES (and DEFINES for INCLUDES)
***************************************************************************** */


#define DEBUG_PRINT_CALCS


/* *****************************************************************************
 *        FIRMWARE VERSION
***************************************************************************** */
#define APPFEAT_VOLUME_CALC_VER_MAJOR 1
#define APPFEAT_VOLUME_CALC_VER_MINOR 0
#define APPFEAT_VOLUME_CALC_BRANCH_MASTER

#define API_TEMPERATURE_MIN_F       (double)-58.0
#define API_TEMPERATURE_MAX_F       (double)302.0

#define API_PRESSURE_MIN_PSIG       (double)0.0
#define API_PRESSURE_MAX_PSIG       (double)1500.0

#define API_DENSITY60_MIN_Kg_m3     (double)610.6
#define API_DENSITY60_MAX_Kg_m3     (double)1163.5

#define API_ALPHA60_MIN_F           (double)230.0E-6
#define API_ALPHA60_MAX_F           (double)930.0E-6

#define DELTA_RHO_CONVERGENCE_VALUE		0.0000001

typedef enum
{
    Round_Increment_Density_API = 0,
    Round_Increment_Density_Relative_Density,
    Round_Increment_Density_kg_m3,
    Round_Increment_Temperature_F,
    Round_Increment_Temperature_C,
    Round_Increment_Pressure_pisg,
    Round_Increment_Pressure_kPa,
    Round_Increment_Pressure_bar,
    Round_Increment_Termal_Expansion_Coef_F,
    Round_Increment_Termal_Expansion_Coef_C,
    Round_Increment_CTL,
    Round_Increment_Scaled_Compressibility_Factor_psi,
    Round_Increment_Scaled_Compressibility_Factor_kPa,
    Round_Increment_Scaled_Compressibility_Factor_bar,
    Round_Increment_CPL,
    Round_Increment_CTPL,
}Round_Increment_Var_Type_t;

typedef enum
{
    API_CALC_STATUS_OK                ,
	API_CALC_STATUS_INPUT_RANGE_ERROR ,
	API_CALC_STATUS_INPUT_DATA_ERROR  ,
	API_CALC_STATUS_CONVERGENCE_ERROR
}API_Calc_Status_t;

typedef enum
{
	COMMODITY_TYPE_CRUDE_OIL,
	COMMODITY_TYPE_FUEL_OIL,
	COMMODITY_TYPE_JET_FUEL,
	COMMODITY_TYPE_TRANSITION_ZONE,
	COMMODITY_TYPE_GASOLINES,
	COMMODITY_TYPE_LUBRIFICATING_OIL,
	COMMODITY_TYPE_NON_SPECIFIED
}Commodity_Type_t;

typedef struct
{
	double Rho60;			// Density At base conditions (60F and 0 psig) (Kg/m^3)
	double Pressure; 		// Pressure at which the observed density was measured (psig)
	double Temperature_F; 	// Temperature at which the observed density was measured (F)
	double Temperature_C; 	// Temperature at which the observed density was measured (C) [Used on BR Method]
	double Density_t_p;	    // Density At observed temperature [Used on BR Method]
	double Volume_t_p;		// Volume at observed conditions (any valid unit is acepted)
	double Alpha60;			// Pre-calculated 60F thermal expansion factor (In case of commodity group not given)
	double Delta60;			// Temperature shift value (a constant, 0.01374979547F
//	double K0;				// Coefficient in correlation for Alpha60 (Kg^2/m^6 F)
//	double K1;				// Coefficient in correlation for Alpha60 (Kg/m^3 F)
//	double K2;				// Coefficient in correlation for Alpha60 (F^-1)
	Commodity_Type_t Commodity_Type;
}Volume_Calc_Input_Data_t;

typedef struct
{
	double Shifted_Temperature_F;	// Alternate teperature shifted to IPTS-68 (F)
	double Shifted_Rho_F;			// Base desnity shifted to IPTS-68 60F basis(Kg/m^3)
	double Alpha60;					// Calculated 60F thermal expansion factor (In case of commodity group given)
	double Rho60;			        // Density At base conditions (60F and 0 psig) (Kg/m^3)
	double Delta_t;					// Alternate temperature minus the base temperature of 60F
	double Ctl;						// Volume correction factor due temperature
	double Fp;						// Scaled compressibility factor(psi^-1)
	double Cpl;						// Volume correction factor due pressure
	double Ctpl;					// Combined volume correction factor due temperature and pressure
	double Ctpl_Rounded;
	double Volume_Base_60;			// Volume at base conditions (60F and 0psig)
	double Rho;						// Density at alternate conditions(Kg/m^3)
	double Density_20_4;			// Density at 20C [Used on BR Method]
	double Volume_20;				// Volume at 20C  [Used on BR Method]
	API_Calc_Status_t Calc_Status;
}Volume_Calc_Output_Data_t;


Volume_Calc_Output_Data_t API_Calc_Inverse_Method(Volume_Calc_Input_Data_t Input_Data);

Volume_Calc_Output_Data_t API_Calc_Mix_Method(Volume_Calc_Input_Data_t Input_Data);

Volume_Calc_Output_Data_t API_Calc_Direct_Method(Volume_Calc_Input_Data_t Input_Data);

Volume_Calc_Output_Data_t BR_Calc_Method(Volume_Calc_Input_Data_t Input_Data);

/* -----------------------------------------------------------------------------
Comp_Density()
        This routine performs the compensation of density of a fluid over the
        temperature influence. This function uses the Brazilian normalization.
--------------------------------------------------------------------------------
Input:  Density_t -> Observed Density of the fluid.
Output: Temperature_t -> Observed temperature of the fluid.
Return: Compensated fluid Density over the temperature of 20 Degree Celcius
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

	B. técn. PETROBRAS , Rio de Janeiro, 38 (3/ 4): 127-137, jul./dez. 1995
----------------------------------------------------------------------------- */
double Comp_Density(double Densidade_Observada, double Temperatura_Observada);

/* -----------------------------------------------------------------------------
Comp_Volume()
        This routine performs the compensation of density of a fluid over the
        temperature influence. This function uses the Brazilian normalization.
--------------------------------------------------------------------------------
Input:  Density_t -> Observed Density of the fluid.
		Temperature_t -> Observed temperature of the fluid.
		Volume_t - > Observed Volume of fluid.
Return: Compensated fluid volume over the temperature of 20 Degree Celcius
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

	B. técn. PETROBRAS , Rio de Janeiro, 38 (3/ 4): 127-137, jul./dez. 1995
----------------------------------------------------------------------------- */
double Comp_Volume(double Densidade_Observada, double Temperatura_Observada, double Volume_Observado);




/* -----------------------------------------------------------------------------
API_Convert_ITS90_To_IPTS-68()
        This routine performs the conversion of temperature ITS90 to IPTS68
--------------------------------------------------------------------------------
Input:  Temperature_ITS_90 -> Temperature on ITS 90 in Degree Fahrenheit.
Return: Converted temperature to IPTS 68 in Degree Fahrenheit.
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 27

NOaRMA PGD 209
----------------------------------------------------------------------------- */

double API_Convert_ITS90_To_IPTS68(double Temperature_ITS_90_Fahrenheit);

/* -----------------------------------------------------------------------------
API_Transfer_Rho60_To_Rho()
        This routine performs the conversion of Rh60 to Rho*
                    (Knowing the Alpha 60)
--------------------------------------------------------------------------------
Input:  Alpha60 -> Thermal expansion factor over 60F
        Delta60 -> Temperature shift value.
        Rho60   -> Density at 60F
Return: Rho*    -> Base density Shifted to IPS-68 60F basis
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 32
----------------------------------------------------------------------------- */

double API_Transfer_Rho60_To_Rho(double Alpha60, double Delta60, double Rho60);

/* -----------------------------------------------------------------------------
API_Transfer_Rho60_To_Rho_No_Alpha60()
        This routine performs the conversion of Rh60 to Rho*
                    (Unknowing the Alpha 60)
--------------------------------------------------------------------------------
Input:  Delta60 -> Temperature shift value.
        Rho60   -> Density at 60F
        K0, K1, K2  -> Coeficients of the fluid
Return: Rho*    -> Base density Shifted to IPS-68 60F basis
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 32
----------------------------------------------------------------------------- */

double API_Transfer_Rho60_To_Rho_No_Alpha60(double Delta60, double Rho60, double K0,
        double K1, double K2);

/* -----------------------------------------------------------------------------
API_Calculate_Alpha60()
        This routine performs the calc of Alpha60 using the commodity group

--------------------------------------------------------------------------------
Input:  K0, K1, K2  -> Coeficients of the fluid
        Rho*    -> Base density Shifted to IPS-68 60F basis

Return: Alpha60     -> Thermal expansion factor over 60F
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calculate_Alpha60(double K0, double K1, double K2, double Rho);

/* -----------------------------------------------------------------------------
API_Calculate_Delta_t()

        This routine performs the calc of Delta_t
--------------------------------------------------------------------------------
Input:  t           -> Alternate Temperature shifted to IPTS-68 basis (F)

Return: Delta_t     -> Alternate Temperature minus the base temperature of 60F
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calculate_Delta_t(double t);

/* -----------------------------------------------------------------------------
API_Calc_C_tl()
        This routine calculate the correction factor due to temperature C_tl

--------------------------------------------------------------------------------
Input:  Delta60 -> Temperature shift value.
        Alpha60 -> Thermal expansion factor over 60F
        Delta_t -> Alternate Temperature minus the base temperature of 60F

Return: C_tl    -> Correction factor due to temperature
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calc_C_tl(double Alpha60, double Delta60, double Delta_t);

/* -----------------------------------------------------------------------------
API_Calc_F_p()
        This routine calculate the scaled compressibility factor F_p.

--------------------------------------------------------------------------------
Input:  t       -> Alternate Temperature shifted to IPTS-68 basis (F)
        Rho*    -> Base density Shifted to IPS-68 60F basis

Return: F_p     -> Scaled compressibility factor
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calc_F_p(double t, double Rho);

/* -----------------------------------------------------------------------------
API_Calc_C_pl()
        This routine calculate the pressure correction factor C_pl.

--------------------------------------------------------------------------------
Input:  F_p     -> Scaled compressibility factor.
        P       -> Alternate Pressure (psig)

Return: C_pl     -> Pressure correction factor
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calc_C_pl(double F_p, double P);

/* -----------------------------------------------------------------------------
API_Calc_C_tpl()
        This routine calculate the combined temperature and pressure
 correction factor C_tpl.

--------------------------------------------------------------------------------
Input:  C_tl     -> Correction factor due to temperature.
        C_pl     -> Pressure correction factor

Return: C_tpl     -> Combined temperature and pressure
 correction factor
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */

double API_Calc_C_tpl(double C_tl, double C_pl);

/* -----------------------------------------------------------------------------
API_Round_Value()

--------------------------------------------------------------------------------
Input:  value           -> Value to be rounded.
        Variable_Type   -> Round Increment type

Return: rounded_value     -> Rounded value
 correction factor
--------------------------------------------------------------------------------
Notes: Check the bellow documentation for more details

NORMA PAG 33
----------------------------------------------------------------------------- */



double API_Round_Value(double value, Round_Increment_Var_Type_t Variable_Type);

#endif /* VOLUME_CALC_H_ */
